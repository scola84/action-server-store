'use strict';

const WriteAction = require('./write');

class UpdateAction extends WriteAction {
  execute(data, request, message) {
    this.messenger.emit('debug', this, 'execute', data, request, message);

    return this
      .write(data)
      .then(this.handleWrite.bind(this, 'update', request, message));
  }
}

module.exports = UpdateAction;

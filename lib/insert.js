'use strict';

const WriteAction = require('./write');

class InsertAction extends WriteAction {
  execute(data, request, message) {
    this.messenger.emit('debug', this, 'execute', data, request, message);

    return this
      .write(data)
      .then(this.handleWrite.bind(this, 'insert', request, message));
  }
}

module.exports = InsertAction;

'use strict';

const AbstractAction = require('./abstract');

class WriteAction extends AbstractAction {
  write() {
    throw new Error('not_implemented');
  }

  handleWrite(type, request, message, result) {
    this.messenger.emit('debug', this, 'handleWrite',
      request, message, result);

    this.messenger.send(message.clone().set('data', result));
    this.messenger.emit(this.getNamespace(), type, request);

    return result;
  }
}

module.exports = WriteAction;

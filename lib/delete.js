'use strict';

const WriteAction = require('./write');

class DeleteAction extends WriteAction {
  execute(data, request, message) {
    this.messenger.emit('debug', this, 'execute', data, request, message);

    return this
      .write(data)
      .then(this.handleWrite.bind(this, 'delete', request, message));
  }
}

module.exports = DeleteAction;

'use strict';

const Diff = require('@scola/diff');
const Error = require('@scola/error');
const AbstractAction = require('./abstract');

class SelectAction extends AbstractAction {
  constructor(validator, database, cache) {
    super(validator, database);
    this.properties.cache = cache;
    this.initialized = false;
  }

  destroy() {
    this.unbindListeners();
    super.destroy();
  }

  setName(name) {
    super.setName(name);
    this.properties.cache.setHash(this.getHash());

    return this;
  }

  setData(data) {
    super.setData(data);
    this.properties.cache.setHash(this.getHash());

    return this;
  }

  execute(data, request, message) {
    this.messenger.emit('debug', this, 'execute',
      data, request, message);

    return this.properties.cache
      .getAll()
      .then(this.handleExecute.bind(this, message))
      .then(this.initialize.bind(this));
  }

  initialize() {
    this.messenger.emit('debug', this, 'initialize');

    if (!this.initialized) {
      this.bindListeners();
      this.initialized = true;
    }
  }

  bindListeners() {
    this.bindListener(this.getNamespace(), this.messenger,
      this.handleChange);
  }

  unbindListeners() {
    this.unbindListener(this.getNamespace(), this.messenger,
      this.handleChange);
  }

  handleExecute(message, cacheData) {
    this.messenger.emit('debug', this, 'handleExecute',
      message, cacheData);

    if (cacheData) {
      return this.handleExecuteResult(message, cacheData);
    }

    return this
      .read(this.properties.data)
      .then(this.handleExecuteRead.bind(this, message));
  }

  read() {
    throw new Error('not_implemented');
  }

  handleExecuteRead(message, dbData) {
    this.messenger.emit('debug', this, 'handleExecuteRead',
      message, dbData);

    return this.properties.cache
      .setAll(dbData)
      .then(this.handleExecuteResult.bind(this, message, dbData));
  }

  handleExecuteResult(message, data) {
    this.messenger.emit('debug', this, 'handleExecuteResult',
      message, data);
    this.messenger.send(message.clone().set('data', data));
  }

  handleChange(type, request) {
    this.messenger.emit('debug', this, 'handleChange',
      type, request);

    this.properties.cache
      .getAll()
      .then(this.handleChangeGet.bind(this, type, request))
      .catch(this.handleChangeError.bind(this, type, request));
  }

  handleChangeGet(type, request, cacheData) {
    this.messenger.emit('debug', this, 'handleChangeGet',
      type, request, cacheData);

    const isChanged = this.check(type, request, cacheData);

    if (!isChanged) {
      return false;
    }

    if (type === 'delete') {
      return this.properties.cache
        .deleteAll()
        .then(this.handleChangeResult.bind(this, type));
    }

    return this
      .read(this.properties.data)
      .then(this.handleChangeRead.bind(this, type, cacheData));
  }

  check() {
    throw new Error('not_implemented');
  }

  handleChangeRead(type, cacheData, dbData) {
    this.messenger.emit('debug', this, 'handleChangeRead',
      type, cacheData, dbData);

    const diff = Diff.calculate(cacheData, dbData);

    return this.properties.cache
      .setAll(dbData)
      .then(this.handleChangeResult.bind(this, type, diff));
  }

  handleChangeResult(type, diff) {
    this.messenger.emit('debug', this, 'handleChangeResult',
      type, diff);

    this.connections.forEach((message) => {
      this.messenger.send(message
        .clone()
        .set('diff', diff)
        .set('type', type)
      );
    });
  }

  handleChangeError(type, request, result, error) {
    this.messenger.emit('error', new Error('action_change_not_handled', {
      origin: error,
      detail: {
        request,
        result,
        type
      }
    }));
  }
}

module.exports = SelectAction;

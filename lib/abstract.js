'use strict';

const Action = require('@scola/action-server');

class AbstractAction extends Action.Abstract {
  constructor(validator, database) {
    super(validator);
    this.database = database;
  }

  getNamespace() {
    return this.properties.name
      .split('.')
      .slice(0, -1)
      .join('.');
  }
}

module.exports = AbstractAction;
